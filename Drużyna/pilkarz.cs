﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Drużyna
{
    class Pilkarz
    {

        public string Imie { get;  set; }
        public string Nazwisko { get; set; }
        public int Waga { get; set; }
        public int Wzrost { get; set; }
        public int Wiek { get; set; }
        public string Klub { get; set; }


        public Pilkarz (string imie, string nazwisko, int wiek, int waga, int wzrost, string klub)
        {
            Imie = imie;
            Nazwisko = nazwisko;
            Wiek = wiek;
            Waga = waga;
            Wzrost = wzrost;
            Klub = klub;
        }

        public override string ToString()
        {
            return $"{Imie} {Nazwisko}: \n\t {Properties.Lang.Lang.Age} {Wiek} {Properties.Lang.Lang.years} \n\t {Properties.Lang.Lang.Weight} {Waga} kg, \n\t {Properties.Lang.Lang.Height} {Wzrost} cm, \n\t {Properties.Lang.Lang.actclub} {Klub}";
        }

        public string DataSave()
        {
            return $"{Imie}_{Nazwisko}_{Wiek}_{Waga}_{Wzrost}_{Klub}";
        }

        public override bool Equals(object obj)
        {
            var p = obj as Pilkarz;

            if (p == null) return false;
            if (p.Imie != this.Imie) return false;
            if (p.Nazwisko != this.Nazwisko) return false;
            if (p.Wiek != this.Wiek) return false;
            if (p.Waga != this.Waga) return false;
            if (p.Klub != this.Klub) return false;
            return true;
        }
    }
}
