﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace Drużyna
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string plik = "repo.txt";

        List<int> ages = new List<int>();
        List<string> clubs = new List<string>();
        public MainWindow()
        {
            InitializeComponent();

            CultureInfo culture = CultureInfo.CurrentCulture;
            actLang.Text = culture.Name;


            for (int i = 16; i < 100; i++)
                ages.Add(i);
            agelist.ItemsSource = ages;

            clubs.Add("Arsenal Londyn");
            clubs.Add("Real Madryt FC");
            clubs.Add("FC Barcelona");
            clubs.Add("Liverpool FC");
            clubs.Add("Borussia Dortmund");

            comboboxClub.ItemsSource = clubs;



        }

        private void delbut_Click(object sender, RoutedEventArgs e)
        {

            var wynik = MessageBox.Show($" {Properties.Lang.Lang.delMessage}", $"{Properties.Lang.Lang.Attention}", MessageBoxButton.YesNoCancel);


            if (wynik == MessageBoxResult.Yes)
            {
                playerlist.Items.Remove(playerlist.SelectedItem);
            }
            else
            {
                if (wynik == MessageBoxResult.No)
                {
                    clearform();
                }
                else if (wynik == MessageBoxResult.Cancel)
                {
                }
            }


        }

        private void modbut_Click(object sender, RoutedEventArgs e)
        {

            if (playerlist.SelectedIndex > -1)
            {
                int wzrst;

                if (int.TryParse(wzrost.Text, out wzrst))
                {
                    if (imie.Text != "" && nazwisko.Text != "" && (string)comboboxClub.SelectedItem != "")
                    {

                        bool nalezy = false;
                        Pilkarz p = new Pilkarz(imie.Text, nazwisko.Text, (int)agelist.SelectedItem, (int)waga.Value, wzrst, (string)comboboxClub.SelectedItem);

                        foreach (var players in playerlist.Items)
                        {
                            if (players.Equals(p))
                            {
                                nalezy = true;
                                MessageBox.Show($" {Properties.Lang.Lang.alreadyExistMessage}");
                                break;
                            }

                        }
                        if (nalezy == false) playerlist.Items[playerlist.SelectedIndex] = p;
                    }
                    else
                    {
                        MessageBox.Show($"{Properties.Lang.Lang.noName}");
                    }
                }
                else
                {
                    MessageBox.Show($"{Properties.Lang.Lang.IncorrectHeight}");
                }
            }
        }

        private void addbut_Click(object sender, RoutedEventArgs e)

        {

            int wzrst;

            if (int.TryParse(wzrost.Text, out wzrst))
            {
                if (imie.Text != "" && nazwisko.Text != "" && (string)comboboxClub.SelectedItem != "")
                {
                    bool nalezy = false;
                    Pilkarz pilkarz = new Pilkarz(imie.Text, nazwisko.Text, (int)agelist.SelectedItem, (int)waga.Value, wzrst, (string)comboboxClub.SelectedItem);

                    foreach (var players in playerlist.Items)
                    {
                        if (players.Equals(pilkarz))
                        {
                            nalezy = true;
                            MessageBox.Show($" {Properties.Lang.Lang.alreadyExistMessage}");
                            break;
                        }

                    }
                    if (nalezy == false)
                    {
                        playerlist.Items.Add(pilkarz);
                        clearform();
                    }


                }
                else
                {
                    MessageBox.Show($"{Properties.Lang.Lang.noName}");
                }
            }
            else
            {
                MessageBox.Show($"{Properties.Lang.Lang.IncorrectHeight}");
            }

        }

        private void clearform()
        {
            imie.Text = "";
            nazwisko.Text = "";
            agelist.SelectedItem = null;
            waga.Value = 40;
            wzrost.Text = "";
            comboboxClub.SelectedItem = null;
        }


        private void waga_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            if (actSetWaga != null)
            {
                actSetWaga.Text = waga.Value + " kg";
            }
        }

        private void playerlist_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (playerlist.SelectedIndex > -1)
            {
                Pilkarz chosenplayer = (Pilkarz)playerlist.SelectedItem;
                imie.Text = chosenplayer.Imie;
                nazwisko.Text = chosenplayer.Nazwisko;
                agelist.SelectedItem = chosenplayer.Wiek;
                waga.Value = chosenplayer.Waga;
                wzrost.Text = chosenplayer.Wzrost.ToString();
                comboboxClub.SelectedItem = chosenplayer.Klub;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            
                if (File.Exists(plik))
                {

                    var linie = File.ReadAllLines(plik);
                    foreach (var linia in linie)
                    {
                        var stringPilkarza = linia.Split('_');
                        Pilkarz p = new Pilkarz(stringPilkarza[0], stringPilkarza[1], int.Parse(stringPilkarza[2]), int.Parse(stringPilkarza[3]), int.Parse(stringPilkarza[4]), stringPilkarza[5]);
                        playerlist.Items.Add(p);
                    }

                }
            }

            private void Window_Closed(object sender, EventArgs e)
            {
                if (File.Exists(plik))
                    File.Delete(plik);

                foreach (Pilkarz p in playerlist.Items)
                    File.AppendAllText(plik, p.DataSave() + Environment.NewLine);

            }

            private void enLang_Click(object sender, RoutedEventArgs e)
            {
                App.changeLanguage("en-EN");
            }
            private void polLang_Click(object sender, RoutedEventArgs e)
            {
                App.changeLanguage("pl-PL");
            }
        }
    }
